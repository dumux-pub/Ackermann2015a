#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://github.com/smuething/dune-multidomain.git
git clone https://gitlab.dune-project.org/extensions/dune-multidomaingrid.git
git clone https://gitlab.dune-project.org/pdelab/dune-pdelab.git
git clone https://gitlab.dune-project.org/staging/dune-typetree.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Ackermann2015a.git

### Go to specific branches
cd dune-common && git checkout releases/2.3 && cd ..
cd dune-geometry && git checkout releases/2.3 && cd ..
cd dune-grid && git checkout releases/2.3 && cd ..
cd dune-istl && git checkout releases/2.3 && cd ..
cd dune-localfunctions && git checkout releases/2.3 && cd ..
cd dune-multidomain && git checkout releases/2.0 && cd ..
cd dune-multidomaingrid && git checkout releases/2.3 && cd ..
cd dune-pdelab && git checkout releases/2.0 && cd ..
cd dune-typetree && git checkout releases/2.3 && cd ..
cd dumux && git checkout releases/2.7 && cd ..

### Go to specific commits
cd dune-common && git checkout e419754cb6c194ef171fdfcacdaca657b07d73f1 && cd ..
cd dune-geometry && git checkout 8e5075fc4c58e116d4a0f003a4d1f0482a3ab618 && cd ..
cd dune-grid && git checkout 5bdc30fc9abb8560c58371d534eb925640aa3e0e && cd ..
cd dune-istl && git checkout 9cd9de65a01e180b521b2a3b2636ffde4f9ec288 && cd ..
cd dune-localfunctions && git checkout eedfe2a7639be4a7e6dc457fc5181454088aebe0 && cd ..
cd dune-multidomain && git checkout b38dcc116153d4e39181f984383cefa7afd8139f && cd ..
cd dune-multidomaingrid && git checkout 1cc143d52e6875e120dbc9d20faad50fee1f9ecd && cd ..
cd dune-pdelab && git checkout f7a7ff4a23e6f43967b006318480ffbf894ff63f && git apply ../Ackermann2015a/pdelab_seqistlsolverbackend-and-diffusionccfv.patch && cd ..
cd dune-typetree && git checkout ecffa10c59fa61a0071e7c788899464b0268719f && cd ..
cd dumux && git checkout 80bd9b3c97d2fcd0191e014e81e01dfff37aaf48 && cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=Ackermann2015a/gcc-optim.opts all
#./dune-common/bin/dunecontrol --opts=Ackermann2015a/clang-optim.opts all
