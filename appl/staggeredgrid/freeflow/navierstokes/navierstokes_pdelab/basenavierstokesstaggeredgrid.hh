/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup NavierStokesStaggeredGrid
 *
 * \brief Local operator base class for staggered grid discretization for steady-state
 * Navier-Stokes equation.
 *
 * The Navier-Stokes-Equations:<br>
 *
 * Mass balance:
 * \f[
 *    \nabla \cdot \left( \varrho_\alpha v_\alpha \right)
 *    - q_{\varrho}
 *    = 0
 * \f]
 *
 * Momentum balance:
 * \f[
 *    \nabla \cdot \left( p_\alpha I
 *      - \varrho_\alpha \nu_\alpha \nabla v_\alpha
 *      \right)
 *    - \varrho_\alpha g
 *    - q_{\varrho v}
 *    = 0
 * \f]
 *
 * \note The transposed part of the viscous term is not included in the
 *       equations yet.
 * \note Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASE_NAVIER_STOKES_STAGGERED_GRID_HH
#define DUMUX_BASE_NAVIER_STOKES_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/grid/common/mcmgmapper.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator base class for staggered grid discretization solving
     * the steady-state Navier-Stokes equation.
     *
     * \tparam BC Boundary condition type
     * \tparam SourceMomentumBalance Source type for velocity
     * \tparam SourceMassBalance Source type for pressure
     * \tparam DirichletVelocity Dirchlet boundary condition for velocity
     * \tparam DirichletPressure Dirchlet boundary condition for pressure
     * \tparam NeumannVelocity Dirchlet boundary condition for velocity
     * \tparam NeumannPressure Dirchlet boundary condition for pressure
     * \tparam GV GridView type
     */
    template<typename BC, typename SourceMomentumBalance, typename SourceMassBalance,
             typename DirichletVelocity, typename DirichletPressure,
             typename NeumannVelocity, typename NeumannPressure, typename GridView>
    class BaseNavierStokesStaggeredGrid
    {
    public:
      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Index of unknowns
      enum { velocityIdx = 0,
             pressureIdx = 1 };

      //! pressure fix corrections
      typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout>
        MapperElement;

      //! \brief Constructor
      BaseNavierStokesStaggeredGrid(const BC& bc_,
                          const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                          const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                          const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                          GridView gridView_)
        : bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_), gridView(gridView_),
          mapperElement(gridView_)
      {
        initialize(gridView);
      }

      /**
       * \brief Contribution to volume integral.
       *
       * Volume integral depending on test and ansatz functions
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v = lfsu.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p = lfsu.template child<pressureIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // /////////////////////
        // geometry information

        // dimension
        static const unsigned int dim = EG::Geometry::dimension;

        // velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<DF, dim>::general(eg.geometry().type()).size(1);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace/2] = curFace % 2;
          faceCentersGlobal[curFace] = eg.geometry().global(faceCentersLocal[curFace]);
        }

        // distance between two face mid points
        Dune::FieldVector<RF, dim> distancesFaceCenters(0.0);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          distancesFaceCenters[curDim] =
            std::abs(faceCentersGlobal[2*curDim+1][curDim] - faceCentersGlobal[2*curDim][curDim]);
        }

        // staggered face volume (goes through cell center) perpendicular to each direction
        Dune::FieldVector<RF, dim> orthogonalFaceVolumes(1.0);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          for (unsigned int normDim = 0; normDim < dim; ++normDim)
          {
            if (curDim != normDim)
            {
              orthogonalFaceVolumes[curDim] *= distancesFaceCenters[normDim];
            }
          }
        }

        // /////////////////////
        // velocities

        // face normal for velocities
        std::vector<Dune::FieldVector<DF,dim> > centerUnitOuterNormals(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          // identify normal with face
          for (typename GridView::IntersectionIterator it = gridView.ibegin(eg.entity());
               it != gridView.iend(eg.entity()); ++it)
          {
            if (Dune::FieldVector<DF, dim>
                (it->geometry().center() - faceCentersGlobal[curFace]).two_norm2() < 1e-14)
            {
              centerUnitOuterNormals[curFace] = it->centerUnitOuterNormal();
            }
          }
        }

        // evaluate shape functions for velocities
        std::vector<std::vector<RangeVelocity> > velocityBasis(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityBasis[curFace].resize(lfsu_v.size());
          lfsu_v.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal[curFace], velocityBasis[curFace]);
        }

        // evaluate velocity on intersection
        std::vector<RangeVelocity> velocityFaces(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityFaces[curFace] = RangeVelocity(0.0);
          for (unsigned int i = 0; i < lfsu_v.size(); i++)
          {
            velocityFaces[curFace].axpy(x(lfsu_v, i), velocityBasis[curFace][i]);
          }
        }

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values
        DF pressure = x(lfsu_p, 0);
#if !COMPRESSIBILITY
        DF density = pressure * 0.0 + DENSITY;
#else
        DF density = pressure / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
#endif // !COMPRESSIBILITY
        DF kinematicViscosity = pressure * 0.0 + KINEMATIC_VISCOSITY;

        // distance weighted average quantities on staggered intersection
        std::vector<RF> velocity_avg(dim);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          velocity_avg[curDim] = 0.5 *
            (velocityFaces[curDim*2][curDim] + velocityFaces[curDim*2+1][curDim]);
        }

        // upwinding on staggered intersection
        std::vector<RF> velocity_up(dim);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          velocity_up[curDim] = velocityFaces[curDim*2][curDim];
          if (velocity_up[curDim] < 0)
          {
            velocity_up[curDim] = velocityFaces[curDim*2+1][curDim];
          }
        }

#if ENABLE_ADVECTION_AVERAGING
        velocity_up = velocity_avg;
#endif // ENABLE_ADVECTION_AVERAGING


        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
            * Contribution to the different balance equations. All formulas for the
            * \b momentum balance equation are given for the left degree of
            * freedom <tt>curDim</tt>.<br>
            *
            * <br>
            * (1) \b Inertia term of \b momentum balance equation
            * \f[
            *    \varrho v v^T
            *    \Rightarrow \int_\gamma \left( \varrho v v^T \right) \cdot n
            * \f]
            * normal case for all coordinate axes
            * \f[
            *    \alpha_\textrm{left}
            *    = |\gamma| \varrho \left[ \left( v v^T \right) \cdot n \right] \cdot n
            *    = |\gamma| \varrho v_\textrm{up,curDim} v_\textrm{avg,curDim}
            * \f]
            * with
            * \f[
            *    v_\textrm{avg} = \frac{v_\textrm{right} \Delta x_\textrm{left}
            *                          + v_\textrm{left} \Delta x_\textrm{right}}
            *                    {\Delta x_\textrm{right} + \Delta x_\textrm{left}}
            * \f]
            *
            * The default value is \b upwinding for the advective part, by
            * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
            * averaging instead of upwinding for \b one velocity component.
            */
#if ENABLE_NAVIER_STOKES
          r.accumulate(lfsu_v, 2*curDim,
                       // normal is always positive
                       1.0 * density
                       * velocity_up[curDim]
                       * velocity_avg[curDim]
                       * orthogonalFaceVolumes[curDim]); // staggered face volume
          r.accumulate(lfsu_v, 2*curDim+1,
                       // normal is always negative
                       -1.0 * density
                       * velocity_up[curDim]
                       * velocity_avg[curDim]
                       * orthogonalFaceVolumes[curDim]); // staggered face volume
#if PRINT_ACCUMULATION_TERM
std::cout << "INERTIA MOMENTUM NORMAL @" <<
" faceCentersGlobal[curDim*2]: " << faceCentersGlobal[curDim*2] <<
" accumulate: " << (1.0 * density
                       * velocity_up[curDim]
                       * velocity_avg[curDim]
                       * orthogonalFaceVolumes[curDim]) <<
std::endl;
std::cout << "INERTIA MOMENTUM NORMAL @" <<
" faceCentersGlobal[curDim*2+1]: " << faceCentersGlobal[curDim*2+1] <<
" accumulate: " << (-1.0 * density
                       * velocity_up[curDim]
                       * velocity_avg[curDim]
                       * orthogonalFaceVolumes[curDim]) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
#endif // ENABLE_NAVIER_STOKES

          /**
           * (2) \b Viscous term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}
           *    = - \nu \rho \nabla v
           *    \Rightarrow \int_\gamma - \boldsymbol{\tau} \cdot n
           *    = \int_\gamma - \nu \rho \nabla v \cdot n
           * \f]
           * normal case for all coordinate axes
           * \f[
           *    \alpha_\textrm{self}
           *    = - |\gamma| \left( \boldsymbol{\tau} \cdot n \right) \cdot n
           *    = - |\gamma| \varrho \nu
           *      \frac{\textrm{d} v_\textrm{curDim}}{\textrm{d} x_\textrm{curDim}}
           *    = - |\gamma| \varrho \nu
           *      \frac{v_{\textrm{right,curDim}} - v_{\textrm{left,curDim}}}
           *           {x_{\textrm{right,curDim}} - x_{\textrm{left,curDim}}}
           * \f]
           */
          r.accumulate(lfsu_v, 2*curDim,
                       // normal is always positive
                       -1.0 * kinematicViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]); // face volume
          r.accumulate(lfsu_v, 2*curDim+1,
                       // normal is always negative
                       1.0 * kinematicViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]); // face volume
#if PRINT_ACCUMULATION_TERM
std::cout << "VISCOUS MOMENTUM NORMAL @" <<
" faceCentersGlobal[curDim*2]: " << faceCentersGlobal[curDim*2] <<
" accumulate: " << (-1.0 * kinematicViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]) <<
std::endl;
std::cout << "VISCOUS MOMENTUM NORMAL @" <<
" faceCentersGlobal[curDim*2+1]: " << faceCentersGlobal[curDim*2+1] <<
" accumulate: " << (1.0 * kinematicViscosity * density
                       * (velocityFaces[curDim*2+1][curDim] - velocityFaces[curDim*2][curDim])
                       / (faceCentersGlobal[curDim*2+1][curDim] - faceCentersGlobal[curDim*2][curDim])
                       * orthogonalFaceVolumes[curDim]) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM

          /**
           * (3) \b Pressure term of \b momentum balance equation (with Gauss theorem)
           *
           * \f[
           *    \nabla p
           *    \Rightarrow \int_\gamma p
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = p \gamma
           * \f]
           */
          r.accumulate(lfsu_v, 2*curDim,
                       // normal is always positive
                       1.0 * pressure
                       * orthogonalFaceVolumes[curDim]); // staggered face volume
          r.accumulate(lfsu_v, 2*curDim+1,
                       // normal is always negative
                       -1.0 * pressure
                       * orthogonalFaceVolumes[curDim]); // staggered face volume
#if PRINT_ACCUMULATION_TERM
std::cout << "PRESSURE MOMENTUM NORMAL @" <<
" faceCentersGlobal[curDim*2]: " << faceCentersGlobal[curDim*2] <<
" accumulate: " << (1.0 * pressure
                       * orthogonalFaceVolumes[curDim]) <<
std::endl;
std::cout << "PRESSURE MOMENTUM NORMAL @" <<
" faceCentersGlobal[curDim*2+1]: " << faceCentersGlobal[curDim*2+1] <<
" accumulate: " << (-1.0 * pressure
                       * orthogonalFaceVolumes[curDim]) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM

          /**
           * (4) \b Source term of \b momentum balance equation<br>
           *
           * \f[
           *    - q_{\varrho v}
           *    \Rightarrow - \int_V q_{\varrho v}
           * \f]
           * \f[
           *    \alpha_\textrm{curDim} = -0.5 q_{\varrho v\textrm{,curDim}} V_e
           * \f]
           */
          RF elementVolume = eg.geometry().volume();

          typename SourceMomentumBalance::Traits::RangeType sourceMomentumBalanceValue_s;
          typename SourceMomentumBalance::Traits::RangeType sourceMomentumBalanceValue_n;
          Dune::GeometryType gt = eg.geometry().type();
          const int qorder = 4;
          const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,qorder);

#if PRINT_ACCUMULATION_TERM
DF temp_accu_0 = 0.0;
DF temp_accu_1 = 0.0;
#endif // PRINT_ACCUMULATION_TERM
          // loop over quadrature points
          for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
          {
            Dune::FieldVector<double, dim> pos_s = it->position();
            Dune::FieldVector<double, dim> pos_n = it->position();
            pos_s[curDim] *= 0.5;
            pos_n[curDim] *= 0.5;
            pos_n[curDim] += 0.5;
            sourceMomentumBalance.evaluate(eg.entity(), pos_s, sourceMomentumBalanceValue_s);
            sourceMomentumBalance.evaluate(eg.entity(), pos_n, sourceMomentumBalanceValue_n);
            r.accumulate(lfsu_v, 2*curDim,
                          -0.5 * sourceMomentumBalanceValue_s[curDim]
                          * elementVolume * it->weight());
            r.accumulate(lfsu_v, 2*curDim+1,
                          -0.5 * sourceMomentumBalanceValue_n[curDim]
                          * elementVolume * it->weight());

#if PRINT_ACCUMULATION_TERM
temp_accu_0 += -0.5 * sourceMomentumBalanceValue_s[curDim]
                       * elementVolume * it->weight();
temp_accu_1 += -0.5 * sourceMomentumBalanceValue_n[curDim]
                       * elementVolume * it->weight();
#endif // PRINT_ACCUMULATION_TERM
         }
#if PRINT_ACCUMULATION_TERM
std::cout << "SOURCE MOMENTUM VOLUME @" <<
" faceCentersGlobal[curDim*2]: " << faceCentersGlobal[curDim*2] <<
" accumulate: " << (temp_accu_0) <<
std::endl;
std::cout << "SOURCE MOMENTUM VOLUME @" <<
" faceCentersGlobal[curDim*2+1]: " << faceCentersGlobal[curDim*2+1] <<
" accumulate: " << (temp_accu_1) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
        }
        /**
          * (5) \b Source term of \b mass balance equation<br>
          *
          * \f[
          *    - q_{\varrho}
          *    \Rightarrow - \int_V q_{\varrho}
          * \f]
          * \f[
          *    \alpha = - q_{\varrho} V_e
          * \f]
          */
        Dune::GeometryType gt = eg.geometry().type();
        const int qorder = 4;
        const Dune::QuadratureRule<DF,dim>& rule = Dune::QuadratureRules<DF,dim>::rule(gt,qorder);

#if PRINT_ACCUMULATION_TERM
DF temp_accu = 0.0;
#endif // PRINT_ACCUMULATION_TERM
        // loop over quadrature points
        for (typename Dune::QuadratureRule<DF,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceMassBalance::Traits::RangeType sourceMassBalanceValue;
          sourceMassBalance.evaluate(eg.entity(), it->position(), sourceMassBalanceValue);
          RF elementVolume = eg.geometry().volume();
          r.accumulate(lfsu_p, 0,
                       -1.0 * sourceMassBalanceValue * elementVolume * it->weight());
#if PRINT_ACCUMULATION_TERM
temp_accu += (-1.0 * sourceMassBalanceValue * elementVolume * it->weight());
#endif // PRINT_ACCUMULATION_TERM
        }
#if PRINT_ACCUMULATION_TERM
std::cout << "SOURCE MASS VOLUME @" <<
" cellCenterGlobal: " << eg.geometry().global(Dune::ReferenceElements<DF, dim>::general(eg.geometry().type()).position(0, 0)) <<
" accumulate: " << (temp_accu) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
      }


      /**
       * \brief Skeleton integral depending on test and ansatz functions.
       *
       * Contribution of flux over interface. Each face is only visited once!
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions of self/inside
       * \param x_s coefficient vector of self/inside
       * \param lfsv_s local function space for test functions of self/inside
       * \param lfsu_n local functions space for ansatz functions of neighbor/outside
       * \param x_n coefficient vector of neighbor/outside
       * \param lfsv_n local function space for test functions of neighbor/outside
       * \param r_s residual vector of self/inside
       * \param r_n residual vector of neighbor/outside
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        const LFSU_V& lfsu_v_n = lfsu_n.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p_s = lfsu_s.template child<pressureIdx>();
        const LFSU_P& lfsu_p_n = lfsu_n.template child<pressureIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // /////////////////////
        // geometry information

        // dimension
        static const unsigned int dim = IG::Geometry::dimension;

        // local position of cell and face centers
        const Dune::FieldVector<DF, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).position(0, 0);
        const Dune::FieldVector<DF, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<DF, dim>::general(ig.outside()->type()).position(0, 0);
        const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).position(0, 0);
//         const Dune::FieldVector<DF, dim>& insideFaceCenterLocal =
//             ig.geometryInInside().global(faceCenterLocal);

        // global position of cell and face centers
        Dune::FieldVector<DF, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);
        Dune::FieldVector<DF, dim> outsideCellCenterGlobal =
          ig.outside()->geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<DF, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<DF, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // face midpoints of all faces
        const unsigned int numFaces =
          Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).size(1);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_s(numFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_n(numFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_s(numFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_n(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<DF, dim>::general(ig.inside()->geometry().type()).position(curFace, 1);
          faceCentersLocal_n[curFace] =
            Dune::ReferenceElements<DF, dim>::general(ig.outside()->geometry().type()).position(curFace, 1);
          faceCentersGlobal_s[curFace] = ig.inside()->geometry().global(faceCentersLocal_s[curFace]);
          faceCentersGlobal_n[curFace] = ig.outside()->geometry().global(faceCentersLocal_n[curFace]);
        }

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        // find out axis parallel to normal vector
        for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
        {
          if (std::abs(faceUnitOuterNormal[curNormDim]) > 1e-10)
          {
            normDim = curNormDim;
          }
        }

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                        * Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).volume();

        // /////////////////////
        // velocities

        // evaluate shape functions at all face midpoints
        std::vector<std::vector<RangeVelocity> > velocityBasis_s(numFaces);
        std::vector<std::vector<RangeVelocity> > velocityBasis_n(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          velocityBasis_s[curFace].resize(lfsu_v_s.size());
          velocityBasis_n[curFace].resize(lfsu_v_n.size());
          lfsu_v_s.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal_s[curFace], velocityBasis_s[curFace]);
          lfsu_v_n.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal_n[curFace], velocityBasis_n[curFace]);
        }

        // evaluate velocity on midpoints of all faces
        std::vector<RangeVelocity> velocities_s(numFaces);
        std::vector<RangeVelocity> velocities_n(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          velocities_s[curFace] = RangeVelocity(0.0);
          velocities_n[curFace] = RangeVelocity(0.0);
          for (unsigned int i = 0; i < lfsu_v_s.size(); i++)
          {
            velocities_s[curFace].axpy(x_s(lfsu_v_s, i), velocityBasis_s[curFace][i]);
            velocities_n[curFace].axpy(x_n(lfsu_v_n, i), velocityBasis_n[curFace][i]);
          }
        }

        // distances between face center and cell centers for rectangular shapes
        DF distanceInsideToFace = std::abs(faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim]);
        DF distanceOutsideToFace = std::abs(outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim]);

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values
        DF pressure_s = x_s(lfsu_p_s, 0);
        DF pressure_n = x_n(lfsu_p_n, 0);
#if !COMPRESSIBILITY
        DF density_s = pressure_s * 0.0 + DENSITY;
        DF density_n = pressure_n * 0.0 + DENSITY;
#else
        DF density_s = pressure_s / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
        DF density_n = pressure_n / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
#endif
        DF kinematicViscosity_s = pressure_s * 0.0 + KINEMATIC_VISCOSITY;
        DF kinematicViscosity_n = pressure_n * 0.0 + KINEMATIC_VISCOSITY;

        //! \todo TODO: remove hard coded gravitional acceleration
//         Dune::FieldVector<DF, dim> gravity(0.0);
//         gravity[dim-1] = -9.81;

        // normal velocity
        DF velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);

        // averaging: distance weighted average quantities on intersection
        std::vector<RangeVelocity> velocities_avg(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            velocities_avg[curFace][curDim] = (distanceInsideToFace * velocities_n[curFace][curDim] + distanceOutsideToFace * velocities_s[curFace][curDim])
                / (distanceInsideToFace + distanceOutsideToFace);
          }
        }
        DF density_avg = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
                         / (distanceInsideToFace + distanceOutsideToFace);
        DF kinematicViscosity_avg = (distanceInsideToFace * kinematicViscosity_n + distanceOutsideToFace * kinematicViscosity_s)
                                    / (distanceInsideToFace + distanceOutsideToFace);

#if ENABLE_DIFFUSION_HARMONIC
        // averaging: harmonic averages for diffusion term
        density_avg = (2.0 * density_n * density_s)
                      / (density_n + density_s);
        kinematicViscosity_avg = (2.0 * kinematicViscosity_n * kinematicViscosity_s)
                                 / (kinematicViscosity_n + kinematicViscosity_s);
#endif // ENABLE_DIFFUSION_HARMONIC

        // upwinding (from self to neighbor)
        DF density_up = density_s;
        std::vector<RangeVelocity> velocities_up(numFaces);
        velocities_up = velocities_s;
        if (velocityNormal < 0)
        {
          density_up = density_n;
          velocities_up = velocities_n;
        }

#if ENABLE_ADVECTION_AVERAGING
        // distance weighted average mean
        density_up = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
                     / (distanceInsideToFace + distanceOutsideToFace);
        velocities_up = velocities_avg;
#endif // ENABLE_ADVECTION_AVERAGING

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call
        Dune::FieldVector<DF, dim> ones(1.0);

        /**
         * Contribution to the different balance equations. All formulas are given for
         * the <tt>self</tt> element.<br>
         * <br>
         * (1) \b Flux term of \b mass balance equation
         *
         * \f[
         *    \varrho v
         *    \Rightarrow \int_\gamma \left( \varrho v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| \varrho_\textrm{up} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for \b the density.
         */
        r_s.accumulate(lfsu_p_s, 0,
                       1.0 * density_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_p_n, 0,
                       -1.0 * density_up
                       * velocityNormal
                       * faceVolume);
#if PRINT_ACCUMULATION_TERM
std::cout << "FLUX MASS NORMAL @" <<
" insideCellCenterGlobal: " << insideCellCenterGlobal <<
" accumulate: " << (1.0 * density_up
                       * velocityNormal
                       * faceVolume) <<
std::endl;
std::cout << "FLUX MASS NORMAL @" <<
" outsideCellCenterGlobal: " << outsideCellCenterGlobal <<
" accumulate: " << (-1.0 * density_up
                       * velocityNormal
                       * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
           * (2) \b Inertia term of \b momentum balance equation
           *
           * \bug think of density averaging also, right now, velocity is averaged,
           *      whereas density is upwinded
           *
           * \f[
           *    \varrho v v^T
           *    \Rightarrow \int_\gamma \left( \varrho v v^T \right) \cdot n
           * \f]
           * Tangential cases for all coordinate axes
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \left[ \left( \varrho v v^T \right) \cdot n \right] \cdot t
           *    = |\gamma| \varrho_\textrm{aou} \left( v \cdot n \right)
           *      \left(v_\textrm{aou} \cdot t \right)
           *    = 0.5 |\gamma| \varrho_\textrm{aou} \left( v \cdot n \right)
           *        v_\textrm{aou,left,t}
           *      + 0.5 |\gamma| \varrho_\textrm{aou} \left( v \cdot n \right)
           *        v_\textrm{aou,right,t}
           * \f]
           * or shorter
           * \f[
           *    \alpha_\textrm{self}
           *    = 0.5 |\gamma| \varrho_\textrm{aou} \left( v \cdot n \right)
           *       \left[ v_\textrm{aou,left,t} + v_\textrm{aou,right,t} \right]
           * \f]
           *
           * The default value is \b upwinding for the advective part, by
           * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
           * averaging instead of upwinding for the tangential velocity component
           * \b and the density.
           */
#if ENABLE_NAVIER_STOKES
          // only tangential case, exclude normal case
          if (curDim != normDim)
          {
            unsigned int tangDim = curDim;
            r_s.accumulate(lfsu_v_s, 2*tangDim,
                          0.5 * density_up
                          * velocityNormal
                          * velocities_up[2*tangDim][tangDim]
                          * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim,
                          -0.5 * density_up
                          * velocityNormal
                          * velocities_up[2*tangDim][tangDim]
                          * faceVolume);
            r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                          0.5 * density_up
                          * velocityNormal
                          * velocities_up[2*tangDim+1][tangDim]
                          * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                          -0.5 * density_up
                          * velocityNormal
                          * velocities_up[2*tangDim+1][tangDim]
                          * faceVolume);

#if PRINT_ACCUMULATION_TERM
std::cout << "INERTIA MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim]: " << faceCentersGlobal_s[2*tangDim] <<
" accu: " << (0.5 * density_up
                          * velocityNormal
                          * velocities_up[2*tangDim][tangDim]
                          * faceVolume) <<
std::endl;
std::cout << "INERTIA MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim]: " << faceCentersGlobal_n[2*tangDim] <<
" accu: " << (-0.5 * density_up
                          * velocityNormal
                          * velocities_up[2*tangDim][tangDim]
                          * faceVolume) <<
std::endl;
std::cout << "INERTIA MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim+1]: " << faceCentersGlobal_s[2*tangDim+1] <<
" accu: " << (0.5 * density_up
                          * velocityNormal
                          * velocities_up[2*tangDim+1][tangDim]
                          * faceVolume) <<
std::endl;
std::cout << "INERTIA MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim+1]: " << faceCentersGlobal_n[2*tangDim+1] <<
" accu: " << (-0.5 * density_up
                          * velocityNormal
                          * velocities_up[2*tangDim+1][tangDim]
                          * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
          }
#endif // ENABLE_NAVIER_STOKES

          /**
           * (3) \b Viscous term of \b momentum balance equation
           *
           * \f[
           *    - \boldsymbol{\tau}
           *    = - \nu \rho \nabla v 
           *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
           *    = - \int_\gamma \nu \rho \left( \nabla v \cdot n \right)
           * \f]
           * Tangential cases for all coordinate axes (given for \b 2-D,
           * \b rectangular grids and \f$n=(0,1)^T\f$ which means balancing
           * momentum for \f$v_\textrm{0}\f$)<br>
           * \f[
           *    \alpha_\textrm{self}
           *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{avg}
           *      \frac{\partial v_{0}}{\partial x_{1}} 
           * \f]
           * Tangential case, for: \f$\frac{\partial v_{0}}{\partial x_{1}}\f$,
           * right and left means along \f$x_1\f$ axis, \f$n\f$ means 1st entry,
           * \f$t\f$ means 0th entry
           * \f[
           *    A
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{avg}
           *      \frac{\partial v_{0}}{\partial x_{1}}
           *    = - |\gamma| \varrho_\textrm{avg} \nu_\textrm{avg}
           *      \left( 0.5 \left[\frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim}
           *           + 0.5 \left[ \frac{\left(v_\textrm{right} - v_\textrm{left} \right|_\textrm{t}}
           *                  {\left| x_\textrm{right} - x_\textrm{left} \right|_\textrm{n}}
           *                \right]_\textrm{curDim+1}
           *      \right)
           * \f]
           *
           * The default procedure for averaging
           * \f$\varrho_\textrm{avg}\f$, \f$\nu_\textrm{avg}\f$ is a distance weighted
           * average, by using the macro <tt>ENABLE_DIFFUSION_HARMONIC 1</tt>
           * one can do an harmonic averaging instead.
           */
          // only tangential case, exclude normal case
          if (curDim != normDim)
          {
            unsigned int tangDim = curDim;
            r_s.accumulate(lfsu_v_s, 2*tangDim,
                           -0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim,
                           0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);

            r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                           -0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);
            r_n.accumulate(lfsu_v_n, 2*tangDim+1,
                           0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume);

#if PRINT_ACCUMULATION_TERM
std::cout << "VISCOUS MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim]: " << faceCentersGlobal_s[2*tangDim] <<
" accu: " << (-0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "VISCOUS MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim]: " << faceCentersGlobal_n[2*tangDim] <<
" accu: " << (0.5 * density_avg * kinematicViscosity_avg
                           *(velocities_n[2*tangDim][tangDim] - velocities_s[2*tangDim][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "VISCOUS MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_s[2*tangDim+1]: " << faceCentersGlobal_s[2*tangDim+1] <<
" accu: " << (-0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "VISCOUS MOMENTUM TANGENTIAL @" <<
" faceCentersGlobal_n[2*tangDim+1]: " << faceCentersGlobal_n[2*tangDim+1] <<
" accu: " << (0.5 * density_avg * kinematicViscosity_avg
                           * (velocities_n[2*tangDim+1][tangDim] - velocities_s[2*tangDim+1][tangDim])
                             / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim])
                           * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
          }
        }
      }

      /**
       * \brief Boundary integral depending on test and ansatz functions
       *
       * We put the Dirchlet evaluation also in the alpha term to save
       * some geometry evaluations.
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions
       * \param x_s coefficient vector
       * \param lfsv_s local function space for test functions
       * \param r_s residual vector
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v_s = lfsu_s.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p_s = lfsu_s.template child<pressureIdx>();
        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();
        typedef typename BC::template Child<pressureIdx>::Type BCPressure;
        const BCPressure& bcPressure = bc.template child<pressureIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // dimensions
        const int dim = IG::dimension;

        // center in face's reference element
        const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<DF, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face properties (coordinates and normal)
        const Dune::FieldVector<DF, dim>& insideFaceCenterLocal =
          ig.geometryInInside().global(faceCenterLocal);
        const Dune::FieldVector<DF,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        // find out axis parallel to normal vector
        for (unsigned int curNormDim = 0; curNormDim < dim; ++curNormDim)
        {
          if (std::abs(faceUnitOuterNormal[curNormDim]) > 1e-10)
          {
            normDim = curNormDim;
          }
        }

        // face volume for integration
        RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
          * Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).volume();

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<DF, dim>::general(ig.inside()->geometry().type()).size(1);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace / 2] = curFace % 2;
          faceCentersGlobal[curFace] =
            ig.inside()->geometry().global(faceCentersLocal[curFace]);
        }

        // cell center in reference element
        const Dune::FieldVector<DF,dim>&
          insideCellCenterLocal = Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).position(0, 0);
        const Dune::FieldVector<DF, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);

        // evaluate transformation which must be linear
        typename IG::Entity::Geometry::JacobianInverseTransposed jac;
        jac = ig.inside()->geometry().jacobianInverseTransposed(insideCellCenterLocal);
        jac.invert();

        // evaluate shape functions for velocities
        std::vector<std::vector<RangeVelocity> > velocityBasis(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityBasis[curFace].resize(lfsu_v_s.size());
          lfsu_v_s.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal[curFace], velocityBasis[curFace]);
        }

        // /////////////////////
        // velocities
        std::vector<RangeVelocity> velocityFaces(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityFaces[curFace] = RangeVelocity(0.0);
          for (unsigned int i = 0; i < lfsu_v_s.size(); i++)
          {
            velocityFaces[curFace].axpy(x_s(lfsu_v_s, i), velocityBasis[curFace][i]);
          }
        }

        // /////////////////////
        // evaluation of unknown

        // pressure in the cell center
        DF pressure_s = x_s(lfsu_p_s, 0);
#if !COMPRESSIBILITY
        DF density_s = pressure_s * 0.0 + DENSITY;
#else
        DF density_s = pressure_s / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
#endif
        // pressure at the boundary given 
        typename DirichletPressure::Traits::RangeType  pressure_boundary(0.0);
        dirichletPressure.evaluateGlobal(faceCenterGlobal, pressure_boundary);

        DF kinematicViscosity_s = pressure_s * 0.0 + KINEMATIC_VISCOSITY;

        // Dirichlet boundary for pressure
        if (bcPressure.isDirichlet(ig, faceCenterLocal))
        {
          /*!
           * Outflow boundary handling for pressure / mass balance<br>
           * (1) \b Flux term of \b mass balance equation
           * \f[
           *    \varrho v
           *    \Rightarrow \int_\gamma \left( \varrho v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_p_s, 0,
                         1.0 * density_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
#if PRINT_ACCUMULATION_TERM
Dune::FieldVector<DF, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);
std::cout << "FLUX MASS DIRICHLET @" <<
" insideCellCenterGlobal: " << insideCellCenterGlobal <<
" accumulate: " << (1.0 * density_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
        }
        else if (bcPressure.isOutflow(ig, faceCenterLocal))
        {
          /*!
           * Outflow boundary handling for pressure / mass balance<br>
           * (1) \b Flux term of \b mass balance equation
           * \f[
           *    \varrho v
           *    \Rightarrow \int_\gamma \left( \varrho v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho \left( v \cdot n \right)
           * \f]
           */
          r_s.accumulate(lfsu_p_s, 0,
                         1.0 * density_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
#if PRINT_ACCUMULATION_TERM
Dune::FieldVector<DF, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);
std::cout << "FLUX MASS OUTFLOW @" <<
" insideCellCenterGlobal: " << insideCellCenterGlobal <<
" accumulate: " << (1.0 * density_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
        }

        // Wall or Inflow boundary for velocity
        if (bcVelocity.isWall(ig, faceCenterLocal) || bcVelocity.isInflow(ig, faceCenterLocal))
        {
          for (unsigned int tangDim = 0; tangDim < dim; ++tangDim)
          {
            // tangential cases only
            if (tangDim != normDim)
            {
              // evaluate boundary condition functions
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCenter;
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner0;
              typename DirichletVelocity::Traits::RangeType dirichletVelocityCorner1;
              Dune::FieldVector<DF, dim> insideFaceCornerLocal0(insideFaceCenterLocal);
              Dune::FieldVector<DF, dim> insideFaceCornerLocal1(insideFaceCenterLocal);
              insideFaceCornerLocal0[tangDim] = 0.0;
              insideFaceCornerLocal1[tangDim] = 1.0;

              dirichletVelocity.evaluate(*(ig.inside()), insideFaceCenterLocal, dirichletVelocityCenter);
              dirichletVelocity.evaluate(*(ig.inside()), insideFaceCornerLocal0, dirichletVelocityCorner0);
              dirichletVelocity.evaluate(*(ig.inside()), insideFaceCornerLocal1, dirichletVelocityCorner1);

              /**
               * Dirichlet boundary handling for velocity/ momentum balance<br>
               * (1) \b Inertia term of \b momentum balance equation
               *
               * \f[
               *    \varrho v v^T
               *    \Rightarrow \int_\gamma \left( \varrho v v^T \right) \cdot n
               * \f]
               * Only the tangential case is regarded here, as if there is a Dirichlet value
               * for the velocity in face normal direction, it is automatically fixed.
               * \f[
               *    \alpha_\textrm{self}
               *    = |\gamma| \left[ \left( \varrho v v^T \right) \cdot n \right] \cdot t
               *    = |\gamma| \varrho \left( v \cdot n \right)
               *      \left(v \cdot t \right)
               *    = 0.5 |\gamma| \varrho \left( v \cdot n \right)
               *        v_\textrm{left,t}
               *      + 0.5 |\gamma| \varrho \left( v \cdot n \right)
               *        v_\textrm{right,t}
               * \f]
               *
               * \todo Currently no upwinding/averaging is done. Once we have variable
               *       density, we should think about this problem again.
               */
#if ENABLE_NAVIER_STOKES
              r_s.accumulate(lfsu_v_s, 2*tangDim,
                            0.5 * density_s
                            * dirichletVelocityCorner0[normDim]
                            * dirichletVelocityCorner0[tangDim]
                            * faceUnitOuterNormal[normDim]
                            * faceVolume);
              r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                            0.5 * density_s
                            * dirichletVelocityCorner1[normDim]
                            * dirichletVelocityCorner1[tangDim]
                            * faceUnitOuterNormal[normDim]
                            * faceVolume);

#if PRINT_ACCUMULATION_TERM
std::cout << "INERTIA MOMENTUM DIRICHLET @" <<
" faceCentersGlobal[2*tangDim]: " << faceCentersGlobal[2*tangDim] <<
" accu: " << (0.5 * density_s * dirichletVelocityCorner0[normDim] * dirichletVelocityCorner0[tangDim]
                         * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "INERTIA MOMENTUM DIRICHLET @" <<
" faceCentersGlobal[2*tangDim+1]: " << faceCentersGlobal[2*tangDim] <<
" accu: " << (0.5 * density_s * dirichletVelocityCorner1[normDim] * dirichletVelocityCorner1[tangDim]
                         * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
#endif // ENABLE_NAVIER_STOKES

              /**
               * (2) \b Viscous term of \b momentum balance equation
               *
               * \f[
               *    - \boldsymbol{\tau}
               *    = - \nu \rho \left( \nabla v \right)
               *    \Rightarrow - \int_\gamma \boldsymbol{\tau} \cdot n
               *    = - \int_\gamma \nu \rho \nabla v \cdot n
               * \f]
               * Only the tangential case is regarded here, as if there is a Dirichlet value
               * for the velocity in face normal direction, it is automatically fixed.
               * \f[
               *    \alpha_\textrm{self}
               *    = - \left( \boldsymbol{\tau} \cdot n \right) \cdot t
               *    = - |\gamma| \varrho \nu
               *      \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}}
               * \f]
               * The first tangential case \f$\left( \frac{\partial v_\textrm{t}}{\partial x_\textrm{n}} \right)\f$,
               * is calculated from the velocities of the perpendicular faces of the
               * inside element and the dirichlet value for the normal velocity.<br>
               *
               * \todo Currently no averaging is done for kinematic viscosity and density.
               *       Once we have variable viscosity and density, we should think
               *       about this problem again.
               */
              r_s.accumulate(lfsu_v_s, 2*tangDim,
                              -0.5 * density_s * kinematicViscosity_s
                              * (velocityFaces[2*tangDim][tangDim] - dirichletVelocityCorner0[tangDim])
                                / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                              * faceUnitOuterNormal[normDim]
                              * faceVolume);

              r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                              -0.5 * density_s * kinematicViscosity_s
                              * (velocityFaces[2*tangDim+1][tangDim] - dirichletVelocityCorner1[tangDim])
                                / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                              * faceUnitOuterNormal[normDim]
                              * faceVolume);

#if PRINT_ACCUMULATION_TERM
std::cout << "VISCOUS MOMENTUM DIRICHLET @" <<
" faceCentersGlobal[2*tangDim]: " << faceCentersGlobal[2*tangDim] <<
" accu: " << (-0.5 * density_s * kinematicViscosity_s
                            * (velocityFaces[2*tangDim][tangDim] - dirichletVelocityCorner0[tangDim])
                              / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
std::cout << "VISCOUS MOMENTUM DIRICHLET @" <<
" faceCentersGlobal[2*tangDim+1]: " << faceCentersGlobal[2*tangDim+1] <<
" accu: " << (-0.5 * density_s * kinematicViscosity_s
                            * (velocityFaces[2*tangDim+1][tangDim] - dirichletVelocityCorner1[tangDim])
                              / (insideCellCenterGlobal[normDim] - faceCenterGlobal[normDim])
                            * faceUnitOuterNormal[normDim] * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
            }
          }
        }
        else if (bcVelocity.isOutflow(ig, faceCenterLocal))
        {
          if (!bcPressure.isDirichlet(ig, faceCenterLocal))
          {
            std::cout << "At faceCenterGlobal " << faceCenterGlobal << "." << std::endl;
            std::cout.flush();
            DUNE_THROW(Dune::Exception, "Pressure has to be Dirichlet, when velocity is Outflow.");
          }
          /**
           * Outflow boundary handling for velocity / momentum balance<br>
           * (1) \b Inertia term of \b momentum balance equation
           *
           * \f[
           *    \varrho v v^T
           *    \Rightarrow \int_\gamma \left( \varrho v v^T \right) \cdot n
           * \f]
           * normal case
           * \f[
           *    \alpha_\textrm{left}
           *    = |\gamma| \varrho \left[ \left( v v \right) \cdot n \right] \cdot n
           *    = |\gamma| \varrho v v n
           * \f]
           *
           * \todo Currently no upwinding/averaging is done. Once we have variable
           *       density, we should think about this problem again.
           */
#if ENABLE_NAVIER_STOKES
          // normal case
          r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                         1.0 * density_s
                         * velocityFaces[ig.indexInInside()][normDim]
                         * velocityFaces[ig.indexInInside()][normDim]
                         * faceUnitOuterNormal[normDim]
                         * faceVolume);

#if PRINT_ACCUMULATION_TERM
Dune::FieldVector<DF, dim> insideCellCenterGlobal =
          ig.inside()->geometry().global(insideCellCenterLocal);
std::cout << "INERTIA MOMENTUM OUTFLOW @" <<
" insideCellCenterGlobal: " << insideCellCenterGlobal <<
" accumulate: " << (1.0 * density_s
                         * velocityFaces[ig.indexInInside()][normDim]
                         * velocityFaces[ig.indexInInside()][normDim]
                         * faceUnitOuterNormal[normDim]
                         * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM

          for (unsigned int tangDim = 0; tangDim < dim; ++tangDim)
          {
            // only tangential cases
            if (tangDim != normDim)
            {
              r_s.accumulate(lfsu_v_s, 2*tangDim,
                            0.5 * density_s
                            * velocityFaces[ig.indexInInside()][normDim]
                            * velocityFaces[2*tangDim][tangDim] // may be use different (then stored) velocity here
                            * faceUnitOuterNormal[normDim]
                            * faceVolume);
              r_s.accumulate(lfsu_v_s, 2*tangDim+1,
                            0.5 * density_s
                            * velocityFaces[ig.indexInInside()][normDim]
                            * velocityFaces[2*tangDim+1][tangDim] // may be use different (then stored) velocity here
                            * faceUnitOuterNormal[normDim]
                            * faceVolume);

#if PRINT_ACCUMULATION_TERM
std::cout << "INERTIA MOMENTUM OUTFLOW @" <<
" faceCentersGlobal[2*tangDim]: " << faceCentersGlobal[2*tangDim] <<
" accu: " << (0.5 * density_s
                           * velocityFaces[ig.indexInInside()][normDim]
                           * velocityFaces[2*tangDim][tangDim] // may be use different (then stored) velocity here
                           * faceUnitOuterNormal[normDim]
                           * faceVolume) <<
std::endl;
std::cout << "INERTIA MOMENTUM OUTFLOW @" <<
" faceCentersGlobal[2*tangDim+1]: " << faceCentersGlobal[2*tangDim] <<
" accu: " << (0.5 * density_s
                           * velocityFaces[ig.indexInInside()][normDim]
                           * velocityFaces[2*tangDim+1][tangDim] // may be use different (then stored) velocity here
                           * faceUnitOuterNormal[normDim]
                           * faceVolume) <<
std::endl;
#endif // PRINT_ACCUMULATION_TERM
            }
          }
#endif // ENABLE_NAVIER_STOKES

          /**
           * Outflow boundary handling for velocity / momentum balance<br>
           * (2) \b Pressure term of \b momentum balance equation
           *
           * \f[
           *    \nabla p
           *    \Rightarrow \int_\gamma p
           * \f]
           * normal case
           * \f[
           *    \alpha_\textrm{left}
           *    = |\gamma| p
           * \f]
           */
          r_s.accumulate(lfsu_v_s, ig.indexInInside(),
                          1.0 * pressure_boundary
                          * faceUnitOuterNormal[normDim]
                          * faceVolume);
        }
        // Symmetry boundary for velocity
        else if (bcVelocity.isSymmetry(ig, faceCenterLocal))
        {
          if (velocityFaces[ig.indexInInside()][normDim] > 1e-6)
          {
            std::cout << "At faceCenterGlobal " << faceCenterGlobal << "." << std::endl;
            std::cout.flush();
            DUNE_THROW(Dune::Exception, "Normal velocity is not 0 at a Symmetry boundary.");
          }
          //! Nothing has to be done in case of Symmetry Condition for velocity.
          //! But ensure the normal velocity is 0.
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * Checks if fundamental assumptions for staggered are valid:
       *  - non-3D
       *  - <tt>curDim</tt>=0 is always the x-axis
       *  - <tt>{curDim+1</tt> \f$ > \f$ <tt>curDim</tt> for all elements (checked
       *    by checking the all face centers)
       *  - opposing face centers have the same coordinate in tangential direction
       *  - equidistant grids
       *
       * Travers grid and store value, which are constant during simulation (like global positions)
       *
       * \tparam GridView GridView type
       */
      void initialize(const GridView& gridView)
      {
        static const double epsilon = 1e-6;
        const int dim = GridView::dimension;
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

#if ENABLE_ADVECTION_AVERAGING // check if upwinding or averaging is used
        std::cout << "ENABLE_ADVECTION_AVERAGING = true" << std::endl;
#endif // ENABLE_ADVECTION_AVERAGING

#if ENABLE_DIFFUSION_HARMONIC // check if harmonic mean is used for diffusion term
        std::cout << "ENABLE_DIFFUSION_HARMONIC = true" << std::endl;
#endif // ENABLE_DIFFUSION_HARMONIC

        // ensure sane dimension
        if ((dim < 1 || dim > 3))
        {
          DUNE_THROW(Dune::NotImplemented,
                     "Staggered grid is only implemented for 1d, 2d, and 3d cases.");
        }

        // check element plausibality
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          // global positions
          const unsigned int numControlVolumeFaces =
            Dune::ReferenceElements<double, dim>::general(eit->geometry().type()).size(1);
          std::vector<Dune::FieldVector<double, dim> > faceCentersGlobal(numControlVolumeFaces);
          for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
          {
            Dune::FieldVector<double, dim> faceCentersLocal;
            std::fill(faceCentersLocal.begin(), faceCentersLocal.end(), 0.5);
            faceCentersLocal[curFace/2] = curFace % 2;
            faceCentersGlobal[curFace] = eit->geometry().global(faceCentersLocal);
          }

          // iterate normal coordinate axis over all dimensions
          for (unsigned int normDim = 0; normDim < dim; ++normDim)
          {
            // check if the coordinate in normal direction of normDim+1 is always
            // greater than the one of normDim
            if (!(faceCentersGlobal[normDim*2+1][normDim]
                  > faceCentersGlobal[normDim*2][normDim]))
            {
              std::cout << "error in element: normDim+1 is not greater than normDim:" <<
                           "measured with epsilon=" << epsilon << "." << std::endl <<
              " the normal coordinate axis is " << normDim <<
              " faceCentersGlobal[normDim*2] " << faceCentersGlobal[normDim*2] <<
              " faceCentersGlobal[normDim*2+1] " << faceCentersGlobal[normDim*2+1] <<
              std::endl <<
              " in " <<  __FILE__ " line " << __LINE__ << std::endl;
              exit(1);
            }

            // iterate tangential coordinate axis over all dimensions expect for normDim
            for (unsigned int tangDim = 0; normDim < dim; ++normDim)
            {
              if (tangDim != normDim)
              {
                // check for normDim always x/y/z-axis
                if (std::abs(faceCentersGlobal[normDim*2+1][tangDim]
                    - faceCentersGlobal[normDim*2][tangDim]) > epsilon)
                {
                  std::cout << "error in element: tangDim coordinate from faceCenters differ, " <<
                    "measured with epsilon=" << epsilon << "." << std::endl <<
                    "the normal coordinate axis is " << normDim << std::endl <<
                    "the tangential coordinate axis is " << tangDim << std::endl <<
                    "faceCentersGlobal[normDim*2] " << faceCentersGlobal[normDim*2] <<
                    " faceCentersGlobal[normDim*2+1] " << faceCentersGlobal[normDim*2+1]
                    << std::endl <<
                    " in " <<  __FILE__ " line " << __LINE__ << std::endl;
                  exit(3);
                }

                // check if faces in subsequent order have the same coordinate in tangential direction
                // and/or if opposing faces have the same coordinate in tangential direction
                if ((faceCentersGlobal[normDim*2][tangDim]
                    > faceCentersGlobal[normDim*2+1][tangDim] + epsilon)
                    || (faceCentersGlobal[normDim*2][tangDim]
                        < faceCentersGlobal[normDim*2+1][tangDim] - epsilon))
                {
                  std::cout << "error in element: two subsequent numbered faces do not have the" <<
                              "same coordinate in tangential direction," <<
                              "measured with epsilon=" << epsilon << "." << std::endl <<
                  " the normal coordinate axis is " << normDim <<
                  " faceCentersGlobal[normDim*2] " << faceCentersGlobal[normDim*2] <<
                  " faceCentersGlobal[normDim*2+1] " << faceCentersGlobal[normDim*2+1] <<
                  std::endl <<
                  "this could have two reasons:" << std::endl <<
                  " (1) two subsequent numbered faces are not opposing" << std::endl <<
                  " (2) two subsequent numbered faces are shifted tangentially" <<
                  std::endl <<
                  " in " <<  __FILE__ " line " << __LINE__ << std::endl;
                  exit(2);
                }
              }
            }
          }
        } // loop over all elements

        // select the two components from the subspaces
        typedef typename BC::template Child<velocityIdx>::Type BCVelocity;
        const BCVelocity& bcVelocity = bc.template child<velocityIdx>();
        typedef typename BC::template Child<pressureIdx>::Type BCPressure;
        const BCPressure& bcPressure = bc.template child<pressureIdx>();

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;
        typedef double DF;

        // loop over grid view to get elemets with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          for (IntersectionIterator ig = gridView.ibegin(*eit);
              ig != gridView.iend(*eit); ++ig)
          {
            // local and global position of face centers
            const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<DF, dim-1>::general(ig->geometry().type()).position(0, 0);
            Dune::FieldVector<DF, dim> faceCenterGlobal = ig->geometry().global(faceCenterLocal);

            // check for multiple defined boundary conditions
            unsigned int numberOfBCTypesAtPos = 0;
            if (bcPressure.isDirichlet(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcPressure.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos > 1)
            {
              std::cout << "BCPressure at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Dirichlet " << bcPressure.isDirichlet(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcPressure.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout.flush();
              DUNE_THROW(Dune::Exception, "Multiple boundary conditions for pressure at one point.");
            }

            // check for multiple defined boundary conditions
            numberOfBCTypesAtPos = 0;
            if (bcVelocity.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcVelocity.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcVelocity.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcVelocity.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos > 1)
            {
              std::cout << "BCVelocity at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcVelocity.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcVelocity.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcVelocity.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcVelocity.isSymmetry(*ig, faceCenterLocal) << std::endl;
              std::cout.flush();
              DUNE_THROW(Dune::Exception, "Multiple boundary conditions for velocity at one point.");
            }
          }
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * Travers grid and store velocites from last Newton step or time step
       * The velocities are copied across each intersection to be available on the other
       * side adjacent face. This is only necessary for the tangential case of the viscous
       * term, which means <tt>dim > 1</tt>.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        // empty on purpose, can be overwritten by child classes
      }

      /**
       * \brief Writes data of an array with element size into textfile
       *
       * Please use the given script <tt>simulationsOutput.sh</tt> to visualize
       * the data with gnuplot. The pictures can be found in the subfolder
       * <tt>pics</tt> after processing.
       *
       * \param time Current time
       * \param timeStep Number of the current time step
       * \param gridCells Number of grid cells of the problem
       */
      void gnuplotElementOutput(double time, unsigned int timeStep, unsigned int gridCells)
      {
        // empty on purpose, can be overwritten by child classes
      }

      /**
       * \brief Writes data of an array with intersection size into textfile
       *
       * Please use the given script <tt>simulationsOutput.sh</tt> to visualize
       * the data with gnuplot. The pictures can be found in the subfolder
       * <tt>pics</tt> after processing.
       *
       * \param time Current time
       * \param timeStep Number of the current time step
       * \param gridCells Number of grid cells of the problem
       */
      void gnuplotIntersectionOutput(double time, unsigned int timeStep, unsigned int gridCells)
      {
        // empty on purpose, can be overwritten by child classes
      }

    private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;
      //! Mapper to get indices for elements
      MapperElement mapperElement;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASE_NAVIER_STOKES_STAGGERED_GRID_HH
