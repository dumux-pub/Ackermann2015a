/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup NavierStokesStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation.
 *
 * The Navier-Stokes-Equations:<br>
 *
 * Mass balance:
 * \f[
 *    \frac{\partial \varrho_\alpha}{\partial t}
 *    + \nabla \cdot \left( \varrho_\alpha v_\alpha \right)
 *    - q_{\varrho}
 *    = 0
 * \f]
 *
 * Momentum balance:
 * \f[
 *    \frac{\partial \left( \varrho_\alpha v_\alpha \right)}{\partial t}
 *    + \nabla \cdot \left( p_\alpha I
 *      - \varrho_\alpha \nu_\alpha
 *        \left( \nabla v_\alpha
 *        + \nabla v_\alpha^T \right)
 *      \right)
 *    - \varrho_\alpha g
 *    - q_{\varrho v}
 *    = 0
 * \f]
 *
 * \note The transposed part of the viscous term is not included in the
 *       equations yet.
 * \note Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_NAVIER_STOKES_STAGGERED_GRID_HH
#define DUMUX_NAVIER_STOKES_STAGGERED_GRID_HH

#include<cstddef>

#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include"basenavierstokesstaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state Navier-Stokes equation.
     *
     * \tparam BC Boundary condition type
     * \tparam SourceMomentumBalance Source type for velocity
     * \tparam SourceMassBalance Source type for pressure
     * \tparam DirichletVelocity Dirchlet boundary condition for velocity
     * \tparam DirichletPressure Dirchlet boundary condition for pressure
     * \tparam NeumannVelocity Dirchlet boundary condition for velocity
     * \tparam NeumannPressure Dirchlet boundary condition for pressure
     * \tparam GV GridView type
     */
    template<typename BC, typename SourceMomentumBalance, typename SourceMassBalance,
             typename DirichletVelocity, typename DirichletPressure,
             typename NeumannVelocity, typename NeumannPressure, typename GridView>
    class NavierStokesStaggeredGrid
    : public NumericalJacobianApplyVolume<NavierStokesStaggeredGrid<
        BC, SourceMomentumBalance, SourceMassBalance, DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure, GridView> >,
      public NumericalJacobianVolume<NavierStokesStaggeredGrid<
        BC, SourceMomentumBalance, SourceMassBalance, DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure, GridView> >,
      public NumericalJacobianSkeleton<NavierStokesStaggeredGrid<
        BC, SourceMomentumBalance, SourceMassBalance, DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure, GridView> >,
      public NumericalJacobianBoundary<NavierStokesStaggeredGrid<
        BC, SourceMomentumBalance, SourceMassBalance, DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure, GridView> >,
      public FullVolumePattern,
      public FullSkeletonPattern,
      public LocalOperatorDefaultFlags,
      public InstationaryLocalOperatorDefaultMethods<>,
      public BaseNavierStokesStaggeredGrid<BC,
        SourceMomentumBalance, SourceMassBalance,
        DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure,
        GridView>
    {
    public:
      typedef BaseNavierStokesStaggeredGrid<BC,
        SourceMomentumBalance, SourceMassBalance,
        DirichletVelocity, DirichletPressure,
        NeumannVelocity, NeumannPressure,
        GridView> ParentType;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Index of unknowns
      enum { velocityIdx = 0,
             pressureIdx = 1 };

      //! \brief Constructor
      NavierStokesStaggeredGrid(const BC& bc_,
                          const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                          const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                          const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                          GridView gridView_)
        : ParentType(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_), gridView(gridView_)
      {
        initialize(gridView);
      }

      /**
       * \brief Contribution to volume integral.
       *
       * This function just calls the ParentType.
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume(const EG& eg, const LFSU& lfsu, const X& x,
                        const LFSV& lfsv, R& r) const
      {
        ParentType::alpha_volume(eg, lfsu, x, lfsv, r);
      }

      /**
       * \brief Skeleton integral depending on test and ansatz functions.
       *
       * This function just calls the ParentType.
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions of self/inside
       * \param x_s coefficient vector of self/inside
       * \param lfsv_s local function space for test functions of self/inside
       * \param lfsu_n local functions space for ansatz functions of neighbor/outside
       * \param x_n coefficient vector of neighbor/outside
       * \param lfsv_n local function space for test functions of neighbor/outside
       * \param r_s residual vector of self/inside
       * \param r_n residual vector of neighbor/outside
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton(const IG& ig,
                          const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                          const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                          R& r_s, R& r_n) const
      {
        ParentType::alpha_skeleton(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n,  lfsv_n, r_s, r_n);
      }

      /**
       * \brief Boundary integral depending on test and ansatz functions
       *
       * This function just calls the ParentType.
       *
       * \tparam IG interface geometry over which the fluxes are evaluated
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param ig interface geometry
       * \param lfsu_s local functions space for ansatz functions
       * \param x_s coefficient vector
       * \param lfsv_s local function space for test functions
       * \param r_s residual vector
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary(const IG& ig, const LFSU& lfsu_s, const X& x_s,
                          const LFSV& lfsv_s, R& r_s) const
      {
        ParentType::alpha_boundary(ig, lfsu_s, x_s, lfsv_s, r_s);
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * This function just calls the ParentType.
       *
       * \tparam GridView GridView type
       */
      void initialize(const GridView& gridView)
      {
        ParentType::initialize(gridView);
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * This function just calls the ParentType.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        ParentType::updateStoredValues(gfs, lastSolution);
      }

//       /**
//        * \brief Writes data of an array with element size into textfile
//        *
//        * Please use the given script <tt>simulationsOutput.sh</tt> to visualize
//        * the data with gnuplot. The pictures can be found in the subfolder
//        * <tt>pics</tt> after processing.
//        *
//        * \param time Current time
//        * \param timeStep Number of the current time step
//        * \param gridCells Number of grid cells of the problem
//        */
//       void gnuplotElementOutput(double time, unsigned int timeStep, unsigned int gridCells)
//       {
//         ParentType::gnuplotElementOutput(time, timeStep, gridCells);
// 
//         const int dim = GridView::dimension;
// 
//         std::stringstream stream("");
//         stream << "gnuplotElement_";
//         stream << std::setprecision(4) << std::setw(4) << std::setfill('0') << gridCells << "-";
//         stream << std::setprecision(4) << std::setw(4) << std::setfill('0') << timeStep << ".out";
//         stream.clear();
//         std::string string(stream.str());
//         const char* fileName = string.c_str();
//         std::fstream file;
//         file.open(fileName, std::ios::out);
//         file << "# time: " << time << std::endl;
//         file << "# ";
// //         for (unsigned int curDim = 0; curDim < dim; ++curDim)
// //         {
// //           file << "global" << curDim << " ";
// //         }
//         for (unsigned int curDim = 0; curDim < dim; ++curDim)
//         {
// //           file << "ElementPressureFixed" << " ";
//           file << "ElementPressureFixed" << "";
//         }
//         file << std::endl;
// 
//         for (unsigned int i = 0; i < this->ElementPressureFixed.size(); ++i)
//         {
// //           for (unsigned int curDim = 0; curDim < dim; ++curDim)
// //           {
// //             file << asImp_().storedElementCentersGlobal[i][curDim] << " ";
// //           }
// //           file << this->ElementPressureFixed[i] << " ";
//           file << this->ElementPressureFixed[i] << "";
//           file << std::endl;
//           if (i == 0) // compatibility for gnuplot use
//           {
//             file << std::endl;
//           }
//         }
//         file.close();
//       }

//       /**
//        * \brief Writes data of an array with intersection size into textfile
//        *
//        * Please use the given script <tt>simulationsOutput.sh</tt> to visualize
//        * the data with gnuplot. The pictures can be found in the subfolder
//        * <tt>pics</tt> after processing.
//        *
//        * \param time Current time
//        * \param timeStep Number of the current time step
//        * \param gridCells Number of grid cells of the problem
//        */
//       void gnuplotIntersectionOutput(double time, unsigned int timeStep, unsigned int gridCells)
//       {
//         ParentType::gnuplotIntersectionOutput(time, timeStep, gridCells);
// 
//         const int dim = GridView::dimension;
// 
//         std::stringstream stream("");
//         stream << "gnuplotIntersection_";
//         stream << std::setprecision(4) << std::setw(4) << std::setfill('0') << gridCells << "-";
//         stream << std::setprecision(4) << std::setw(4) << std::setfill('0') << timeStep << ".out";
//         stream.clear();
//         std::string string(stream.str());
//         const char* fileName = string.c_str();
//         std::fstream file;
//         file.open(fileName, std::ios::out);
//         file << "# time: " << time << std::endl;
//         file << "# ";
//         for (unsigned int curDim = 0; curDim < dim; ++curDim)
//         {
//           file << "global" << curDim << " ";
//         }
// //         file << "storedCorrespondingWallElementID" << " ";
// //         file << "storedDistanceToWall" << " ";
//         file << std::endl;
// 
//         for (unsigned int i = 0; i < storedIntersectionCentersGlobal.size(); ++i)
//         {
//           for (unsigned int curDim = 0; curDim < dim; ++curDim)
//           {
//             file << asImp_().storedElementCentersGlobal[i][curDim] << " ";
//           }
// //           file << storedCorrespondingWallElementID[i] << " ";
// //           file << storedDistanceToWall[i] << " ";
//           if (i == 0) // compatibility for gnuplot use
//           {
//             file << std::endl;
//           }
//         }
//         file.close();
//       }

    private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_NAVIER_STOKES_STAGGERED_GRID_HH
