/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup NavierStokesStaggeredGrid
 *
 * \brief Base class for transient part of staggered grid discretization for
 *        Navier-Stokes equation.
 *
 * The Navier-Stokes-Equations:<br>
 *
 * Mass balance:
 * \f[
 *    \frac{\partial}{\partial t} \varrho
 *    + \dots
 *    = 0
 * \f]
 *
 * Momentum balance:
 * \f[
 *    \frac{\partial}{\partial t} \left( \varrho v \right)
 *    + \dots
 *    = 0
 * \f]
 */

#ifndef DUMUX_BASE_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_HH
#define DUMUX_BASE_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/grid/common/mcmgmapper.hh>

#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include <dune/geometry/quadraturerules.hh>

#include "basenavierstokesstaggeredgrid.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator base class for staggered grid discretization solving
     * the transient part of the Navier-Stokes equation.
     */
    template<typename GridView>
    class BaseNavierStokesTransientStaggeredGrid
    {
    public:
      //! \brief pattern assembly flags
      enum { doPatternVolume = true };

      //! \brief residual assembly flags
      enum { doAlphaVolume = true };

      //! \brief Index of unknowns
      enum { velocityIdx = 0,
             pressureIdx = 1 };

      //! \brief Constructor
      BaseNavierStokesTransientStaggeredGrid(GridView gridView_)
        : gridView(gridView_)
      { }

      //! set time for subsequent evaluation
      void setTime (double t)
      {
        time = t;
      }

      /**
       * \brief Contribution to volume integral.
       *
       * Volume integral depending on test and ansatz functions
       *
       * \tparam EG element geometry
       * \tparam LFSU local function space for ansatz functions
       * \tparam X coefficient vector
       * \tparam LFSV local function space for test functions
       * \tparam R residual vector
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       * \param lfsv local function space for test functions
       * \param r residual vector
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume (const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r) const
      {
        // select the two components from the subspaces
        typedef typename LFSU::template Child<velocityIdx>::Type LFSU_V;
        const LFSU_V& lfsu_v = lfsu.template child<velocityIdx>();
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_P& lfsu_p = lfsu.template child<pressureIdx>();

        // domain and range field type
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::DomainFieldType DF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeFieldType RF;
        typedef typename LFSU_V::Traits::FiniteElementType::
          Traits::LocalBasisType::Traits::RangeType RangeVelocity;

        // /////////////////////
        // geometry information

        // dimension
        static const unsigned int dim = EG::Geometry::dimension;

#if PRINT_ACCUMULATION_TERM
        // local position of cell center and cell volume
        const Dune::FieldVector<DF, dim>& cellCenterLocal =
          Dune::ReferenceElements<DF, dim>::general(eg.geometry().type()).position(0, 0);
        Dune::FieldVector<DF, dim> cellCenterGlobal =
          eg.geometry().global(cellCenterLocal);
#endif // PRINT_ACCUMULATION_TERM

        const RF elementVolume = eg.geometry().volume();

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<DF, dim>::general(eg.geometry().type()).size(1);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace/2] = curFace % 2;
          faceCentersGlobal[curFace] = eg.geometry().global(faceCentersLocal[curFace]);
        }

        // /////////////////////
        // velocities

        // face normal for velocities
        std::vector<Dune::FieldVector<DF,dim> > centerUnitOuterNormals(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          // identify normal with face
          for (typename GridView::IntersectionIterator it = gridView.ibegin(eg.entity());
               it != gridView.iend(eg.entity()); ++it)
          {
            if (Dune::FieldVector<DF, dim>
                (it->geometry().center() - faceCentersGlobal[curFace]).two_norm2() < 1e-14)
            {
              centerUnitOuterNormals[curFace] = it->centerUnitOuterNormal();
            }
          }
        }

        // evaluate shape functions for velocities
        std::vector<std::vector<RangeVelocity> > velocityBasis(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityBasis[curFace].resize(lfsu_v.size());
          lfsu_v.finiteElement().localBasis().evaluateFunction(
            faceCentersLocal[curFace], velocityBasis[curFace]);
        }

        // evaluate velocity on intersection
        std::vector<RangeVelocity> velocityFaces(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          velocityFaces[curFace] = RangeVelocity(0.0);
          for (unsigned int i = 0; i < lfsu_v.size(); i++)
          {
            velocityFaces[curFace].axpy(x(lfsu_v, i), velocityBasis[curFace][i]);
          }
        }

        // /////////////////////
        // evaluation of unknown

        // evaluate cell values
        DF pressure = x(lfsu_p, 0);
#if !COMPRESSIBILITY
        DF density = pressure * 0.0 + DENSITY;
#else
        DF density = pressure / (SPECIFIC_GAS_CONSTANT * TEMPERATURE);
#endif

        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension

        /**
          * (1) \b Storage term of \b mass balance equation
          * \f[
          *    \frac{\partial}{\partial t} varrho
          * \f].
          */
        r.accumulate(lfsu_p, 0,
                      density * elementVolume);
#if PRINT_ACCUMULATION_TERM
std::cout << "STORAGE MASS VOLUME @" <<
" cellCenterGlobal: " << cellCenterGlobal <<
" accumulate: " << (density * elementVolume) << std::endl;
#endif // PRINT_ACCUMULATION_TERM

        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          /**
           * (2) \b Storage term of \b momentum balance equation
           * \f[
           *    \frac{\partial}{\partial t} \left( \varrho v \right)
           * \f].
           */
          r.accumulate(lfsu_v, 2*curDim,
                       density
                       * velocityFaces[curDim*2][curDim]
                       * elementVolume * 0.5); // half cell volume, other half for other volume
          r.accumulate(lfsu_v, 2*curDim+1,
                       density
                       * velocityFaces[curDim*2+1][curDim]
                       * elementVolume * 0.5); // half cell volume, other half for other volume
#if PRINT_ACCUMULATION_TERM
std::cout << "STORAGE MOMENTUM VOLUME @" <<
" faceCentersGlobal[curDim*2]: " << faceCentersGlobal[curDim*2] <<
" accumulate: " << (density
                       * velocityFaces[curDim*2][curDim]
                       * elementVolume * 0.5) << std::endl;
std::cout << "STORAGE MOMENTUM VOLUME @" <<
" faceCentersGlobal[curDim*2+1]: " << faceCentersGlobal[curDim*2+1] <<
" accumulate: " << (density
                       * velocityFaces[curDim*2+1][curDim]
                       * elementVolume * 0.5) << std::endl;
#endif // PRINT_ACCUMULATION_TERM
        }
      }

    private:
      GridView gridView;
      //! Instationary variables
      double time;
    };
  } // end namespace PDELab
} // end namespace Dune

#endif // DUMUX_BASE_NAVIER_STOKES_TRANSIENT_STAGGERED_GRID_HH
