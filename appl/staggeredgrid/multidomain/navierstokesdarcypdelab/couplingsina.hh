#ifndef DUMUX_MULTIDOMAIN_PDELAB_COUPLING_SINA_HH
#define DUMUX_MULTIDOMAIN_PDELAB_COUPLING_SINA_HH

#include <dune/geometry/quadraturerules.hh>

#include <dune/pdelab/localoperator/idefault.hh>
#include <dune/pdelab/localoperator/pattern.hh>

#include <dune/pdelab/multidomain/couplingutilities.hh>

/**
 * \tparam TReal Scalar value type
 */

template<typename TReal>
class CouplingSina :
        public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags,
        public Dune::PDELab::MultiDomain::NumericalJacobianCoupling<CouplingSina<TReal> >,
        public Dune::PDELab::MultiDomain::NumericalJacobianApplyCoupling<CouplingSina<TReal> >,
        public Dune::PDELab::MultiDomain::FullCouplingPattern,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<TReal>
        {
        public:

            static const bool doAlphaCoupling = true;
            static const bool doPatternCoupling = true;

            template<typename IG, typename LFSU1, typename LFSU2, typename X,
            typename LFSV1, typename LFSV2, typename R>
            void alpha_coupling(const IG& ig, const LFSU1& lfsu_s, const X& x_s, const LFSV1& lfsv_s,
                    const LFSU2& lfsu_n, const X& x_n, const LFSV2 lfsv_n, R& r_s, R& r_n) const
            {
                // domain and range field type
                typedef typename LFSU1::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits::DomainFieldType DF;
                typedef typename LFSU1::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits::RangeFieldType RF;
                typedef typename LFSU1::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits::RangeType RangeType;
                typedef typename LFSU1::Traits::FiniteElementType::
                        Traits::LocalBasisType::Traits::JacobianType JacobianType;

                // dimension
                static const unsigned int dim = IG::Geometry::dimension;

                // local position of face center and face normal
                const Dune::FieldVector<DF, dim-1>& faceCenterLocal =
                        Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).position(0, 0);
                const Dune::FieldVector<DF, dim>& faceUnitOuterNormal =
                        ig.centerUnitOuterNormal();

                // evaluate orientation of intersection
                unsigned int normDim = 0;
                unsigned int tangDim = 1;
                for (unsigned int curDim = 0; curDim < dim; ++curDim)
                {
                    if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
                    {
                        normDim = curDim;
                        tangDim = 1 - curDim;
                    }
                }

                // face midpoints of all faces
                const unsigned int numFaces =
                        Dune::ReferenceElements<DF, dim>::general(ig.inside()->type()).size(1);
                std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_s(numFaces);
                std::vector<Dune::FieldVector<DF, dim> > faceCentersLocal_n(numFaces);
                std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_s(numFaces);
                std::vector<Dune::FieldVector<DF, dim> > faceCentersGlobal_n(numFaces);
                for (unsigned int curFace = 0; curFace < numFaces; ++curFace) {
                    faceCentersLocal_s[curFace] =
                            Dune::ReferenceElements<DF, dim>::general(ig.inside()->geometry().type()).position(curFace, 1);
                faceCentersLocal_n[curFace] =
                                         Dune::ReferenceElements<DF, dim>::general(ig.inside()->geometry().type()).position(curFace, 1);
                faceCentersGlobal_s[curFace] =
                                         ig.inside()->geometry().global(faceCentersLocal_s[curFace]);
                faceCentersGlobal_n[curFace] =
                                         ig.inside()->geometry().global(faceCentersLocal_n[curFace]);
                }

                // cell centers in references elements
                const Dune::FieldVector<DF,IG::dimension>&
                  inside_local = Dune::ReferenceElements<DF,IG::dimension>::general(ig.inside()->type()).position(0,0);
                const Dune::FieldVector<DF,IG::dimension>&
                  outside_local = Dune::ReferenceElements<DF,IG::dimension>::general(ig.outside()->type()).position(0,0);

                // cell centers in global coordinates
                Dune::FieldVector<DF,IG::dimension>
                  inside_global = ig.inside()->geometry().global(inside_local);
                Dune::FieldVector<DF,IG::dimension>
                  outside_global = ig.outside()->geometry().global(outside_local);

                // distance between the two cell centers
                inside_global -= outside_global;
                RF deltaX = inside_global.two_norm();

                // face volume for integration
                RF faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                        * Dune::ReferenceElements<DF, dim-1>::general(ig.geometry().type()).volume();

                // evaluate shape functions at all face midpoints
                std::vector<std::vector<RangeType> > velocityBasis_s(numFaces);
                std::vector<std::vector<RangeType> > velocityBasis_n(numFaces);
                for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
                {
                    velocityBasis_s[curFace].resize(lfsu_s.size());
                    lfsu_s.finiteElement().localBasis().evaluateFunction(
                            faceCentersLocal_s[curFace], velocityBasis_s[curFace]);

                    velocityBasis_n[curFace].resize(lfsu_n.size());
                    lfsu_n.finiteElement().localBasis().evaluateFunction(
                            faceCentersLocal_n[curFace], velocityBasis_n[curFace]);
                }
                // evaluate velocity on midpoints of all faces
                std::vector<RangeType> velocities_s(numFaces);
                std::vector<RangeType> velocities_n(numFaces);
                for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
                {
                    velocities_s[curFace] = RangeType(0.0);
                    for (unsigned int i = 0; i < lfsu_s.size(); i++)
                    {
                        velocities_s[curFace].axpy(x_s(lfsu_s,i), velocityBasis_s[curFace][i]);
                    }
                    velocities_n[curFace] = RangeType(0.0);
                    for (unsigned int i = 0; i < lfsu_n.size(); i++)
                    {
                        velocities_n[curFace].axpy(x_n(lfsu_n,i), velocityBasis_n[curFace][i]);
                    }
                }

                DF jump = velocities_s[ig.indexInInside()] - velocities_n[ig.indexInInside()];

                // boundary conditions
                // Neumann b.c. --> flux over interface
                r_s.accumulate(lfsu_s, ig.indexInInside(),
                        faceUnitOuterNormal[normDim] * jump *faceVolume / deltaX);

                // Dirichlet b.c. --> value in neighboring cell across interface
                r_s.accumulate(lfsu_n, ig.indexInInside(), velocities_s[ig.indexInInside()]);


            }
        };
#endif // DUMUX_MULTIDOMAIN_PDELAB_COUPLING_SINA_HH
