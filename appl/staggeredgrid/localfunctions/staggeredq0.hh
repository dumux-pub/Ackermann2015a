/** \file
 *  \ingroup StaggeredLocalfunctions
 *
 *  \brief Base class for staggered Q0 finite elements
*/

#ifndef DUMUX_STAGGERED_Q0_LOCALFINITEELEMENT_HH
#define DUMUX_STAGGERED_Q0_LOCALFINITEELEMENT_HH

#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/localfiniteelementtraits.hh>

#include "staggeredq0localbasis.hh"
#include "staggeredq0localcoefficients.hh"
#include "staggeredq0localinterpolation.hh"


namespace Dune 
{
  /**
   * \brief Staggered Q0 finite elements, like Q0 but with jump in
   *        middle of element.
   *
   * \tparam DF Domain data type
   * \tparam RF Range data type
   * \tparam dim Dimension of the reference element
   */
  template<class DF, class RF, int dim>
  class StaggeredQ0LocalFiniteElement 
  {
  public:
    typedef LocalFiniteElementTraits<
      StaggeredQ0LocalBasis<DF, RF, dim>,
      StaggeredQ0LocalCoefficients<dim>,
      StaggeredQ0LocalInterpolation<StaggeredQ0LocalBasis<DF, RF, dim> > > Traits;

    /**
     * \brief Constructor
     */
    StaggeredQ0LocalFiniteElement()
    {
      gt.makeCube(dim);
    }

    /**
     * \brief Copy constructor
     */
    StaggeredQ0LocalFiniteElement(const StaggeredQ0LocalFiniteElement& o)
      : gt(o.gt)
    {}

    /**
     * \brief local basis
     */
    const typename Traits::LocalBasisType& localBasis() const
    {
      return basis;
    }

    /**
     * \brief local coefficients
     */
    const typename Traits::LocalCoefficientsType& localCoefficients() const
    {
      return coefficients;
    }

    /**
     * \brief local interpolation
     */
    const typename Traits::LocalInterpolationType& localInterpolation() const
    {
      return interpolation;
    }

    /**
     * \brief geometry type
     */
    GeometryType type() const
    {
      return gt;
    }

    /**
     * \brief Return a copy of this local FE
     */
    StaggeredQ0LocalFiniteElement* clone() const
    {
      return new StaggeredQ0LocalFiniteElement(*this);
    }

  private:
    StaggeredQ0LocalBasis<DF, RF, dim> basis;
    StaggeredQ0LocalCoefficients<dim> coefficients;
    StaggeredQ0LocalInterpolation<StaggeredQ0LocalBasis<DF, RF, dim> > interpolation;
    GeometryType gt;
  };
}

#endif // DUMUX_STAGGERED_Q0_LOCALFINITEELEMENT_HH
