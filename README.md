Summary
=======

This is the DuMuX module containing the code for producing the results
published in:

S. Ackermann<br>
Analysis of the iterative Dirichlet-Neumann coupling method for the Poisson problem<br>
Forschungsmodul II, Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart, 2015.


Installation
============

You can build the module just like any other DUNE module. For building and running the executables, please go to the folders
containing the sources listed below. For the basic dependencies see dune-project.org. Note that you have to have the
BOOST library installed for this module.  

The easiest way to install this module is to create a new folder and to execute the file
[installAckermann2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Ackermann2015a/raw/master/installAckermann2015a.sh)
in this folder. 

```bash
mkdir -p Ackermann2015a && cd Ackermann2015a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Ackermann2015a/raw/master/installAckermann2015a.sh
sh ./installAckermann2015a.sh
```
For a detailed information on installation have a look at the DuMuX installation guide or use the DuMuX handbook, chapter 2.


Applications
============

The results shown in the publication are obtained by compiling and running the program from the source given below,
which can be found in appl/staggeredgrid/multidomain/navierstokesdarcypdelab/:

test_diffusionccfvDNcoupled.cc


Output
======

Run test_diffusionccfvDNcoupled with the respective source file modifications and ini-files, which can be found in the ini subfolder in the root directory:

__Figure 3.1b - Problem setup__: use "0_Initial_problem_setup.ini"

__Figure 3.2 - Computing time__:  
    - change Dirichlet boundary condition to y = x[0] * x[1]; (line 42)  
    - use "ComputingTime_a_i.ini", i = 1,...,7  
    - for the results of theta = 0.99: change line 42 in the ini file to "0.99"  

__Figure 3.3 - Computing time__:  
    - change Dirichlet boundary condition to y = x[0] * x[1]; (line 42)  
    - use "ComputingTime_b_i.ini", i = 1,...,5  
    - for the results of theta = 0.99: change line 42 in the ini file to "0.99"  

__Figure 3.4 - Convergence__: use "Convergence_i.ini", i = 1,...,7

__Figure 3.5, 3.6 - Inexact solution__: use "Inexact_1e-i.ini", i = 4,...,14

__Figure 3.7 - Different linear solvers__:  
    - change linear solver in line 593 to "GradientSolver"  
    - use "0_Initial_problem_setup.ini"

__Figure 3.8 - 3D solution__:  
    - use Dirichlet boundary condition in line 44  
    - change line 245 to "const int dim = 3;"  
    - uncomment line 251 (high[2] = parameters.get<double>("mesh.x_length");)  
    - uncomment line 255 (n[2] = parameters.get<int>("mesh.x_refine");)  
    - use "0_Initial_problem_setup.ini"
    
Used Versions and Software
==========================
For an overview of the used versions of the DUNE and DuMuX modules, please have a look at
[installAckermann2015a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Ackermann2015a/raw/master/installAckermann2015a.sh).